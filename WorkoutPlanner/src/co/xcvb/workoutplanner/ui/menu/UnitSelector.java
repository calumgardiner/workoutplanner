package co.xcvb.workoutplanner.ui.menu;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import co.xcvb.workoutplanner.data.units.WeightUnit;

public class UnitSelector extends ComboBox<WeightUnit> {

	private ObservableList<WeightUnit> options = FXCollections.observableArrayList(WeightUnit.values());

	public UnitSelector() {
		this.setItems(options);
		this.getSelectionModel().select(UnitsSelected.getInstance().getUnitsSelected());
	}

}
