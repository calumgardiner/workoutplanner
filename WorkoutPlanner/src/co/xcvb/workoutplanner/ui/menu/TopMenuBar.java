package co.xcvb.workoutplanner.ui.menu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import co.xcvb.workoutplanner.ui.WPApplication;

public class TopMenuBar extends MenuBar {

	private Menu fileMenu;
	private Menu optionsMenu;
	private Menu helpMenu;
	private Menu statsMenu;
	private WPApplication application;

	public TopMenuBar(WPApplication application) {
		this.application = application;
		setupMenuBar();
	}

	private void setupMenuBar() {
		fileMenu = new Menu("File");
		MenuItem open = new MenuItem("Open");
		open.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				application.loadFile();
			}
		});
		MenuItem save = new MenuItem("Save");
		save.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				application.saveToFile();
			}
		});
		MenuItem close = new MenuItem("Close");
		close.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				application.close();
			}
		});

		fileMenu.getItems().addAll(open, save, close);

		optionsMenu = new Menu("Options");

		helpMenu = new Menu("Help");

		statsMenu = new Menu("Statistics");
		MenuItem viewStats = new MenuItem("View Stats");
		viewStats.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				application.showStatsWindow();
			}
		});
		MenuItem viewGraphs = new MenuItem("View Graphs");
		viewGraphs.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				application.showGraphsWindow();
			}
		});
		statsMenu.getItems().addAll(viewStats, viewGraphs);
		this.getMenus().addAll(fileMenu, optionsMenu, statsMenu, helpMenu);
	}

}
