package co.xcvb.workoutplanner.data.stored;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class ExerciseStore {

	private static final String FILE_STORE = "./resources/StoredExercises.xml";
	private StoredExercises store;

	public ExerciseStore() {

	}

	public void loadStore() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(StoredExercises.class);
		Unmarshaller un = context.createUnmarshaller();
		this.store = (StoredExercises) un.unmarshal(new File(FILE_STORE));
	}

	public void saveStoreToFile() throws Exception {
		if (store != null) {
			JAXBContext context = JAXBContext.newInstance(StoredExercises.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(store, new File(FILE_STORE));
		} else {
			throw new Exception("Cannot save empty store.");
		}
	}

	public StoredExercises getStore() {
		return store;
	}

	public void setStoredExercises(StoredExercises store) {
		this.store = store;
	}

}
