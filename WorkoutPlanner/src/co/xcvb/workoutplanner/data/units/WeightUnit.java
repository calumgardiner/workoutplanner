package co.xcvb.workoutplanner.data.units;

/**
 * <p>
 * Enum containing weight units and a conversion rate from each to KG.
 * </p>
 * 
 *
 */
public enum WeightUnit {

	/**
	 * Kilograms
	 */
	KG(1.0d, "Kilograms"),
	/**
	 * Pounds
	 */
	LBS(2.20462d, "Pounds");

	private double conversionRate;
	private String description;

	WeightUnit(double conversionRate, String description) {
		this.conversionRate = conversionRate;
		this.description = description;
	}

	public double getConversionRateToKG() {
		return this.conversionRate;
	}

	public String getDescription() {
		return this.description;
	}
}
