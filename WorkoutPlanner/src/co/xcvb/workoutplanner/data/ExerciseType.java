package co.xcvb.workoutplanner.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExerciseType implements Serializable {

	private static final long serialVersionUID = 7481312785194678681L;
	private String name;
	private String image;

	public ExerciseType() {

	}

	public ExerciseType(String name, String imageLocation) {
		this.name = name;
		this.image = imageLocation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String toString() {
		return this.name;
	}

	public boolean equals(Object type) {
		if (type instanceof ExerciseType) {
			return ((ExerciseType) type).getName().equals(this.getName());
		}
		return false;
	}

}
