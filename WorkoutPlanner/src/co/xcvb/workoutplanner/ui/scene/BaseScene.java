package co.xcvb.workoutplanner.ui.scene;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

public class BaseScene extends Scene {

	private static BorderPane rootPane = new BorderPane();

	public BaseScene() {
		super(rootPane, 1000, 800);
		this.getStylesheets().add("file:./resources/style.css");
	}

	public BorderPane getRootPane() {
		return BaseScene.rootPane;
	}

}
