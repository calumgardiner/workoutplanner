package co.xcvb.workoutplanner.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import co.xcvb.workoutplanner.data.lifting.WSet;

public class Exercise implements Serializable {

	private static final long serialVersionUID = 8561898126728604834L;
	private ExerciseType type;
	private List<WSet> sets;

	public Exercise() {
		this.sets = new ArrayList<WSet>();
	}

	public Exercise(ExerciseType type) {
		this.type = type;
		this.sets = new ArrayList<WSet>();
	}

	public ExerciseType getType() {
		return type;
	}

	public void setType(ExerciseType type) {
		this.type = type;
	}

	public List<WSet> getSets() {
		return sets;
	}

	public void setSets(List<WSet> sets) {
		this.sets = sets;
	}

}
