package co.xcvb.workoutplanner.data.calculation;

public enum Calculation {
	
	MAX_1RM,
	AVERAGE_WEIGHT_LIFTED;

}
