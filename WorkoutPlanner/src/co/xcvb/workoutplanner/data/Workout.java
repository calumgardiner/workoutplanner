package co.xcvb.workoutplanner.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Workout implements Serializable {

	private static final long serialVersionUID = -4967078313845744703L;
	private Date date;
	private List<Exercise> exercises;

	public Workout() {
		this.setExercises(new ArrayList<Exercise>());
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Exercise> getExercises() {
		return exercises;
	}

	public void setExercises(List<Exercise> exercises) {
		this.exercises = exercises;
	}

}
