package co.xcvb.workoutplanner.ui.pane;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import co.xcvb.workoutplanner.data.Exercise;
import co.xcvb.workoutplanner.data.Workout;

public class WorkoutPane extends BorderPane {

	private Workout workout;
	private Button addExercise;
	private VBox exercises;

	public WorkoutPane(Workout workout) {
		this.setPadding(new Insets(10));
		this.workout = workout;
		this.addExercise = new Button("Add Exercise");
		VBox addButtonBox = new VBox(addExercise);
		addButtonBox.setPadding(new Insets(5, 0, 0, 0));
		this.setBottom(addButtonBox);
		this.exercises = new VBox();
		ScrollPane scrollPane = new ScrollPane(exercises);
		scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollPane.setStyle("-fx-background-color:transparent;");
		this.setCenter(scrollPane);
		if (workout.getExercises() != null && !workout.getExercises().isEmpty()) {
			for (Exercise e : workout.getExercises()) {
				addExercisePane(e);
			}
		}
		this.addExercise.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				Exercise e = new Exercise();
				workout.getExercises().add(e);
				addExercisePane(e);
			}
		});
		// resize the exercises when the pane is resized
		this.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				exercises.setPrefWidth(newValue.doubleValue() - 20);
			}
		});
	}

	public void addExercisePane(Exercise exercise) {
		ExercisePane pane = new ExercisePane(exercise);
		this.exercises.getChildren().add(pane);
		pane.getDeleteExerciseButton().setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				exercises.getChildren().remove(pane);
				workout.getExercises().remove(exercise);
			}
		});
	}

}
