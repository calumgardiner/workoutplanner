package co.xcvb.workoutplanner.graph;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import co.xcvb.workoutplanner.data.ExerciseType;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.calculation.WorkoutUtil;
import co.xcvb.workoutplanner.data.lifting.WSet;
import co.xcvb.workoutplanner.data.units.WeightConverter;
import co.xcvb.workoutplanner.data.units.WeightUnit;

public class GoogleGraphWorkoutData {

	private static final DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

	public static String getMaxLiftsByDateForExercise(List<Workout> workouts, Date from, Date to, ExerciseType type,
			WeightUnit unit) {
		// filter to only dates we need
		List<Workout> workoutsByDate = WorkoutUtil.getAllWorkoutsBetweenDates(from, to, workouts);
		// Get map with dates where the workout happened with the heaviest set
		// on that date
		Map<Date, WSet> heaviestSetsByDate = WorkoutUtil.getHeaviestSetForEachDate(workoutsByDate, type);
		String data = "";
		data += "['Date', ";
		data += "'" + type.getName() + "'],\n";
		for (Entry<Date, WSet> dateSetTuple : heaviestSetsByDate.entrySet()) {
			data += "['" + df.format(dateSetTuple.getKey()) + "', ";
			data += ""
					+ WeightConverter.convertWeight(dateSetTuple.getValue().getWeight(), dateSetTuple.getValue()
							.getUnitOfWeight(), unit) + "],";
		}
		data = data.substring(0, data.length() - 1);
		return data;
	}

}
