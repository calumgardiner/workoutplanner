package co.xcvb.workoutplanner.data.stored;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import co.xcvb.workoutplanner.data.Workout;

public class WorkoutIO {

	/**
	 * Save a list of workouts to the file path specified.
	 * 
	 * @param workouts
	 * @param filePath
	 */
	public static void saveToFile(List<Workout> workouts, String filePath) throws Exception {
		try (OutputStream file = new FileOutputStream(filePath);
				OutputStream buffer = new BufferedOutputStream(file);
				ObjectOutput output = new ObjectOutputStream(buffer);) {
			output.writeObject(workouts);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Load the list of workouts from the file path specified.
	 * 
	 * @param filePath
	 * @return workouts in file
	 */
	@SuppressWarnings("unchecked")
	public static List<Workout> loadFromFile(String filePath) throws Exception {
		List<Workout> recoveredQuarks = null;
		try (InputStream file = new FileInputStream(filePath);
				InputStream buffer = new BufferedInputStream(file);
				ObjectInput input = new ObjectInputStream(buffer);) {
			Object readObject = input.readObject();
			recoveredQuarks = (List<Workout>) readObject;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return recoveredQuarks;
	}
}
