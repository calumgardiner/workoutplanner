package co.xcvb.workoutplanner.data.stored;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import co.xcvb.workoutplanner.data.ExerciseType;

@XmlRootElement
public class StoredExercises {

	private List<ExerciseType> exercises;

	public void setExercises(List<ExerciseType> exercises) {
		this.exercises = exercises;
	}

	public List<ExerciseType> getExercises() {
		return this.exercises;
	}

}
