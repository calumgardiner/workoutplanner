package co.xcvb.workoutplanner.ui.menu;

import co.xcvb.workoutplanner.data.units.WeightUnit;

public class UnitsSelected {

	private static WeightUnit selectedUnit;
	private static UnitsSelected instance;

	private UnitsSelected() {
		selectedUnit = WeightUnit.KG;
	}

	public static UnitsSelected getInstance() {
		if (instance == null) {
			instance = new UnitsSelected();
		}
		return instance;
	}

	public WeightUnit getUnitsSelected() {
		return selectedUnit;
	}

	public void setUnitsSelected(WeightUnit unit) {
		selectedUnit = unit;
	}

}
