package co.xcvb.workoutplanner.ui.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MessageDialog extends Stage {

	private Button close;
	private static MessageDialog instance;
	private static BorderPane group;

	public static MessageDialog getInstance(String message) {
		if (instance == null) {
			instance = new MessageDialog();
		}
		group.setCenter(new Text(message));
		return instance;
	}

	private MessageDialog() {
		close = new Button("Close");
		group = new BorderPane();
		group.setPadding(new Insets(10));
		HBox buttons = new HBox();
		buttons.setAlignment(Pos.CENTER);
		buttons.getChildren().add(close);
		group.setBottom(buttons);
		Scene createWorkoutScene = new Scene(group);
		this.setResizable(false);
		this.setHeight(150);
		this.setWidth(300);
		this.initStyle(StageStyle.UNDECORATED);
		this.setAlwaysOnTop(true);
		this.setScene(createWorkoutScene);
		group.setBackground(new Background(new BackgroundFill(Color.GAINSBORO, CornerRadii.EMPTY, Insets.EMPTY)));
		this.setTitle("New Workout");
		setButtonActions();
	}

	private void setButtonActions() {
		this.close.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				MessageDialog.this.hide();
			}
		});
	}
}
