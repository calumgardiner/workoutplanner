package co.xcvb.workoutplanner.ui.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.ui.WPApplication;

public class DatesButtons extends HBox {

	private WPApplication application;
	private Button addWorkout;
	private Button deleteWorkout;
	private Button copyWorkout;

	public DatesButtons(WPApplication application) {
		this.application = application;
		this.addWorkout = new Button("+");
		this.deleteWorkout = new Button("-");
		this.copyWorkout = new Button("Copy");
		this.addWorkout.setTooltip(new Tooltip("Add Workout"));
		this.deleteWorkout.setTooltip(new Tooltip("Remove Workout"));
		this.copyWorkout.setTooltip(new Tooltip("Copy Workout"));
		this.addWorkout.setPrefWidth(50);
		this.deleteWorkout.setPrefWidth(50);
		this.copyWorkout.setPrefWidth(50);
		setupDialogs();
		this.setSpacing(10);
		this.getChildren().add(addWorkout);
		this.getChildren().add(deleteWorkout);
		this.getChildren().add(copyWorkout);
	}

	private void setupDialogs() {
		addWorkout.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				CreateWorkoutDialog.getInstance(application, null).show();
			}
		});
		deleteWorkout.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				Workout selectedWorkout = DatesButtons.this.application.getSelectedWorkout();
				if (selectedWorkout != null) {
					DatesButtons.this.application.removeWorkout(selectedWorkout);
				}
			}
		});
		copyWorkout.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				Workout selectedWorkout = DatesButtons.this.application.getSelectedWorkout();
				if (selectedWorkout != null) {
					CreateWorkoutDialog.getInstance(application, selectedWorkout).show();
				}
			}
		});
	}

}
