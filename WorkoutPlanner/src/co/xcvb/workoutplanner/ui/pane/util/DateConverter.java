package co.xcvb.workoutplanner.ui.pane.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateConverter {

	public static Date getDateFromLocalDate(LocalDate local) {
		return Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

}
