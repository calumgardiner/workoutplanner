package co.xcvb.workoutplanner.data.units;

/**
 * <p>
 * Static methods for converting between weight units.
 * </p>
 * 
 *
 */
public class WeightConverter {

	/**
	 * <p>
	 * Convert weigth from unit to another.
	 * 
	 * @param weight
	 * @param currentUnit
	 * @param requiredUnit
	 * @return weight in required unit
	 */
	public static double convertWeight(double weight, WeightUnit currentUnit, WeightUnit requiredUnit) {
		double weightInKG = weight / currentUnit.getConversionRateToKG();
		double weightInRequired = weightInKG * requiredUnit.getConversionRateToKG();
		return weightInRequired;
	}

}
