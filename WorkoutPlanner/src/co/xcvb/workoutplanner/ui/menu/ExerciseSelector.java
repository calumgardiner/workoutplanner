package co.xcvb.workoutplanner.ui.menu;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import javax.xml.bind.JAXBException;

import co.xcvb.workoutplanner.data.ExerciseType;
import co.xcvb.workoutplanner.data.stored.ExerciseStore;

public class ExerciseSelector extends ComboBox<ExerciseType> {

	private ObservableList<ExerciseType> options;
	private ExerciseStore store;

	public ExerciseSelector() {
		this.store = new ExerciseStore();
		reloadStore();
		setCellFactory();
		this.setPromptText("Exercise...");
	}

	private void setCellFactory() {
		this.setCellFactory(new Callback<ListView<ExerciseType>, ListCell<ExerciseType>>() {
			@Override
			public ListCell<ExerciseType> call(ListView<ExerciseType> arg0) {
				ListCell<ExerciseType> cell = new ListCell<ExerciseType>() {
					@Override
					public void updateItem(ExerciseType item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null) {
							setText(item.getName());
							ImageView icon = new ImageView(item.getImage());
							icon.setSmooth(true);
							icon.setPreserveRatio(true);
							icon.setFitHeight(200);
							Tooltip mouseOver = new Tooltip();
							mouseOver.setGraphic(icon);
							setTooltip(mouseOver);
						} else {
							setText(null);
						}
					}
				};
				return cell;
			}
		});
	}

	public void reloadStore() {
		try {
			this.store.loadStore();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		this.options = FXCollections.observableArrayList(store.getStore().getExercises());
		this.getItems().removeAll();
		this.getItems().addAll(options);
	}

}
