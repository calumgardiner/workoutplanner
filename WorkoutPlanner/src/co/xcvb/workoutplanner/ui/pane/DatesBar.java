package co.xcvb.workoutplanner.ui.pane;

import java.text.DateFormat;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.ui.WPApplication;
import co.xcvb.workoutplanner.ui.pane.util.WorkoutComparator;

public class DatesBar extends ListView<Workout> {

	private static DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
	private WPApplication application;
	private ObservableList<Workout> workouts;

	public DatesBar(WPApplication application) {
		this.application = application;
		this.workouts = FXCollections.observableArrayList();
		this.setEditable(false);
		this.setItems(workouts);
		setCellFactory();
		setMouseClicked();
	}

	public void createWorkoutDate(Workout workout) {
		workouts.add(workout);
		workouts.sort(new WorkoutComparator());
	}

	public void removeWorkoutDate(Workout workout) {
		workouts.remove(workout);
		workouts.sort(new WorkoutComparator());
	}

	private void setMouseClicked() {
		this.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				Workout selected = DatesBar.this.getSelectionModel().getSelectedItem();
				DatesBar.this.application.showWorkout(selected);
			}
		});
	}

	private void setCellFactory() {
		this.setCellFactory(new Callback<ListView<Workout>, ListCell<Workout>>() {
			@Override
			public ListCell<Workout> call(ListView<Workout> arg0) {
				ListCell<Workout> cell = new ListCell<Workout>() {
					@Override
					protected void updateItem(Workout workout, boolean empty) {
						super.updateItem(workout, empty);
						setText(workout != null ? df.format(workout.getDate()) : "");
					}
				};
				return cell;
			}
		});
	}

	public List<Workout> getWorkouts() {
		return this.workouts;
	}
	
	public void setWorkouts(List<Workout> workouts) {
		this.workouts.clear();
		for (Workout w : workouts) {
			this.createWorkoutDate(w);
		}
	}

}
