package co.xcvb.workoutplanner.data.clone;

import java.util.Date;

import co.xcvb.workoutplanner.data.Exercise;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.lifting.WSet;

public class WorkoutCopy {
	
	public static Workout copyWorkout(Workout workout) {
		Workout copy = new Workout();
		copy.setDate(new Date(workout.getDate().getTime()));
		for (Exercise exercise : workout.getExercises()) {
			Exercise eCopy = new Exercise(exercise.getType());
			for (WSet set : exercise.getSets()) {
				WSet copySet = new WSet(set.getReps().intValue(), set.getWeight().doubleValue(), set.getUnitOfWeight());
				eCopy.getSets().add(copySet);
			}
			copy.getExercises().add(eCopy);
		}
		return copy;
	}

}
