package co.xcvb.workoutplanner.ui.pane;

import java.time.LocalDate;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.clone.WorkoutCopy;
import co.xcvb.workoutplanner.ui.WPApplication;
import co.xcvb.workoutplanner.ui.pane.util.DateConverter;

public class CreateWorkoutDialog extends Stage {

	private DatePicker workoutDate;
	private WPApplication application;
	private Button close;
	private Button create;
	private static CreateWorkoutDialog instance;
	private Workout workoutToCopy;

	public static CreateWorkoutDialog getInstance(WPApplication application, Workout workoutToCopy) {
		if (instance == null) {
			instance = new CreateWorkoutDialog(application);
		}
		instance.application = application;
		instance.workoutToCopy = workoutToCopy;
		return instance;
	}

	private CreateWorkoutDialog(WPApplication application) {
		workoutDate = new DatePicker(LocalDate.now());
		close = new Button("Close");
		create = new Button("Create");
		BorderPane group = new BorderPane();
		group.setPadding(new Insets(10));
		group.setTop(new Text(25, 25, "Select Workout Date: "));
		group.setCenter(workoutDate);
		HBox buttons = new HBox();
		buttons.setAlignment(Pos.CENTER);
		buttons.getChildren().add(create);
		buttons.getChildren().add(close);
		group.setBottom(buttons);
		Scene createWorkoutScene = new Scene(group);
		this.setResizable(false);
		this.setHeight(150);
		this.setWidth(300);
		this.initStyle(StageStyle.UNDECORATED);
		this.setAlwaysOnTop(true);
		this.setScene(createWorkoutScene);
		group.setBackground(new Background(new BackgroundFill(Color.GAINSBORO, CornerRadii.EMPTY, Insets.EMPTY)));
		this.setTitle("New Workout");
		setButtonActions();
	}

	private void setButtonActions() {
		this.close.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				CreateWorkoutDialog.this.hide();
			}
		});

		this.create.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				CreateWorkoutDialog.this.createWorkout();
				CreateWorkoutDialog.this.hide();
			}
		});
	}

	private void createWorkout() {
		Workout workout;
		if (workoutToCopy != null) {
			workout = WorkoutCopy.copyWorkout(workoutToCopy);
		} else {
			workout = new Workout();
		}
		Date date = DateConverter.getDateFromLocalDate(this.workoutDate.getValue());
		workout.setDate(date);
		this.application.createWorkout(workout);
	}
}
