package co.xcvb.workoutplanner.data.lifting;

import java.io.Serializable;

import co.xcvb.workoutplanner.data.units.WeightUnit;

/**
 * <p>
 * Weight set, contains reps and weight done in KG.
 * </p>
 * 
 * @author calum
 *
 */
public class WSet implements Serializable {

	private static final long serialVersionUID = -1731325506751683010L;
	private Integer reps;
	private Double weight;
	private WeightUnit unit;

	public WSet(Integer reps, Double weight, WeightUnit unit) {
		this.reps = reps;
		this.weight = weight;
		this.unit = unit;
	}

	public void setReps(Integer reps) {
		this.reps = reps;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public void setUnit(WeightUnit unit) {
		this.unit = unit;
	}

	public Integer getReps() {
		return this.reps;
	}

	public Double getWeight() {
		return this.weight;
	}

	public WeightUnit getUnitOfWeight() {
		return unit;
	}

}
