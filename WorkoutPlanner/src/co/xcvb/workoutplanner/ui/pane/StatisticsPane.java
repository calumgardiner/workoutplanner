package co.xcvb.workoutplanner.ui.pane;

import java.time.LocalDate;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.calculation.Calculation;
import co.xcvb.workoutplanner.data.calculation.WorkoutUtil;
import co.xcvb.workoutplanner.ui.WPApplication;
import co.xcvb.workoutplanner.ui.menu.ExerciseSelector;
import co.xcvb.workoutplanner.ui.menu.UnitSelector;
import co.xcvb.workoutplanner.ui.pane.util.DateConverter;

public class StatisticsPane extends BorderPane {

	private DatePicker fromDate;
	private DatePicker toDate;
	private WPApplication application;
	private List<Workout> workouts;
	private ExerciseSelector exerciseTypeSelector;
	private UnitSelector unitsSelector;
	private Button go;
	private ComboBox<Calculation> calculationToDo;
	private Text result;

	public StatisticsPane(WPApplication application, List<Workout> workouts) {
		this.workouts = workouts;
		this.application = application;
		this.setPadding(new Insets(10));
		fromDate = new DatePicker(LocalDate.now());
		fromDate.setPrefWidth(110);
		toDate = new DatePicker(LocalDate.now());
		toDate.setPrefWidth(110);
		exerciseTypeSelector = new ExerciseSelector();
		exerciseTypeSelector.getSelectionModel().select(0);
		unitsSelector = new UnitSelector();
		calculationToDo = new ComboBox<Calculation>();
		calculationToDo.setItems(FXCollections.observableArrayList(Calculation.values()));
		calculationToDo.getSelectionModel().select(0);
		result = new Text("Select the options above and press go to calculate.");
		result.setFont(new Font(25));
		go = new Button("Go");
		HBox controlsTop = new HBox();
		controlsTop.setSpacing(5);
		HBox.setHgrow(controlsTop, Priority.ALWAYS);
		controlsTop.getChildren().add(fromDate);
		controlsTop.getChildren().add(new Text("to"));
		controlsTop.getChildren().add(toDate);
		controlsTop.getChildren().add(exerciseTypeSelector);
		controlsTop.getChildren().add(unitsSelector);
		controlsTop.getChildren().add(calculationToDo);
		controlsTop.getChildren().add(go);
		this.setTop(controlsTop);
		this.setCenter(result);
		setGoAction();
	}

	private void setGoAction() {
		go.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				switch (calculationToDo.getSelectionModel().getSelectedItem()) {
				case AVERAGE_WEIGHT_LIFTED:
					Double average = WorkoutUtil.getAverageLiftBetweenDates(exerciseTypeSelector.getSelectionModel()
							.getSelectedItem(), DateConverter.getDateFromLocalDate(fromDate.getValue()), DateConverter
							.getDateFromLocalDate(toDate.getValue()), workouts, unitsSelector.getSelectionModel()
							.getSelectedItem());
					result = new Text("Your average "
							+ exerciseTypeSelector.getSelectionModel().getSelectedItem().getName() + " was "
							+ Math.round(average) + " " + unitsSelector.getSelectionModel().getSelectedItem()
							+ " between the dates above.");
					result.setFont(new Font(20));
					StatisticsPane.this.setCenter(result);
					break;
				case MAX_1RM:
					Double max1RM = WorkoutUtil.get1RMForLift(exerciseTypeSelector.getSelectionModel()
							.getSelectedItem(), DateConverter.getDateFromLocalDate(fromDate.getValue()), DateConverter
							.getDateFromLocalDate(toDate.getValue()), workouts, unitsSelector.getSelectionModel()
							.getSelectedItem());
					result = new Text("Your Max 1RM "
							+ exerciseTypeSelector.getSelectionModel().getSelectedItem().getName() + " was "
							+ Math.round(max1RM) + " " + unitsSelector.getSelectionModel().getSelectedItem()
							+ " between the dates above.");
					result.setFont(new Font(20));
					StatisticsPane.this.setCenter(result);
					break;
				default:
					break;

				}
			}
		});
	}

}
