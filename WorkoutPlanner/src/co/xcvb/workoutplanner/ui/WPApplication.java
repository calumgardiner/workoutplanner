package co.xcvb.workoutplanner.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.stored.WorkoutIO;
import co.xcvb.workoutplanner.graph.WorkoutGraph;
import co.xcvb.workoutplanner.ui.menu.TopMenuBar;
import co.xcvb.workoutplanner.ui.pane.DatesBar;
import co.xcvb.workoutplanner.ui.pane.DatesButtons;
import co.xcvb.workoutplanner.ui.pane.MessageDialog;
import co.xcvb.workoutplanner.ui.pane.StatisticsPane;
import co.xcvb.workoutplanner.ui.pane.WorkoutPane;
import co.xcvb.workoutplanner.ui.scene.BaseScene;

public class WPApplication extends Application {

	public static void main(String... args) {
		launch();
	}

	private Stage stage;
	private BaseScene scene;
	private TopMenuBar menu;
	private DatesBar datesBar;
	private DatesButtons datesButtons;
	private ExtensionFilter nattyExtension = new ExtensionFilter("Natty Workout", "*.natty");

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		this.stage.setTitle("Workout Recorder");
		this.scene = new BaseScene();
		this.menu = new TopMenuBar(this);
		this.datesBar = new DatesBar(this);
		this.datesButtons = new DatesButtons(this);
		this.scene.getRootPane().setTop(menu);
		VBox leftBox = new VBox();
		leftBox.getChildren().add(datesBar);
		leftBox.getChildren().add(datesButtons);
		leftBox.setPadding(new Insets(10));
		leftBox.setSpacing(5);
		VBox.setVgrow(datesBar, Priority.ALWAYS);
		this.scene.getRootPane().setLeft(leftBox);
		this.stage.setScene(scene);
		this.stage.show();
	}

	public void showWorkout(Workout workout) {
		if (workout != null) {
			this.scene.getRootPane().setCenter(new WorkoutPane(workout));
		}
	}

	public void createWorkout(Workout workout) {
		this.datesBar.createWorkoutDate(workout);
	}

	public void removeWorkout(Workout workout) {
		this.datesBar.removeWorkoutDate(workout);
	}

	public void setScene(Scene scene) {
		this.stage.setScene(scene);
		this.stage.show();
	}

	public Workout getSelectedWorkout() {
		return datesBar.getSelectionModel().getSelectedItem();
	}

	public void close() {
		Platform.exit();
	}

	public Stage getPrimaryStage() {
		return this.stage;
	}

	public void loadFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(nattyExtension);
		fileChooser.setTitle("Open Workout File");
		File file = fileChooser.showOpenDialog(stage);
		String filePath = file.getAbsolutePath();
		List<Workout> workoutsFromFile;
		try {
			workoutsFromFile = WorkoutIO.loadFromFile(filePath);
			this.datesBar.setWorkouts(workoutsFromFile);
		} catch (Exception e) {
			MessageDialog.getInstance("Something went wrong when loading the file.").show();
		}
	}

	public void saveToFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(nattyExtension);
		fileChooser.setTitle("Save Workout File");
		File file = fileChooser.showSaveDialog(stage);
		String filePath = file.getAbsolutePath();
		try {
			List<Workout> serializableList = new ArrayList<Workout>();
			for (Workout workout : this.datesBar.getWorkouts()) {
				serializableList.add(workout);
			}
			WorkoutIO.saveToFile(serializableList, filePath);
			MessageDialog.getInstance("Saved file successfully.").show();
		} catch (Exception e) {
			MessageDialog.getInstance("Something went wrong when saving.").show();
		}
	}

	public void showStatsWindow() {
		this.scene.getRootPane().setCenter(new StatisticsPane(this, this.datesBar.getWorkouts()));
	}
	
	public void showGraphsWindow() {
		this.scene.getRootPane().setCenter(new WorkoutGraph(this.datesBar.getWorkouts()));
	}

}
