package co.xcvb.workoutplanner.ui.pane;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import co.xcvb.workoutplanner.data.lifting.WSet;
import co.xcvb.workoutplanner.data.units.WeightUnit;
import co.xcvb.workoutplanner.ui.menu.UnitSelector;

public class SetPane extends BorderPane {

	private Button deleteSet;
	private static final int HEIGHT = 30;

	public SetPane(WSet set) {
		this.deleteSet = new Button("x");
		this.deleteSet.setTextAlignment(TextAlignment.CENTER);
		this.deleteSet.setMinWidth(15);
		this.deleteSet.setMinHeight(HEIGHT);
		this.deleteSet.setTooltip(new Tooltip("Delete Set"));

		setMargin(deleteSet, new Insets(0,5,0,0));
		HBox repsAndWeight = new HBox();
		TextField reps = new TextField();
		reps.setEditable(true);
		reps.setText(set.getReps().toString());
		reps.setMaxWidth(35);
		reps.setMinHeight(HEIGHT);
		repsAndWeight.getChildren().add(reps);
		TextField at = new TextField("@");
		at.setPadding(new Insets(3));
		at.setMaxWidth(20);
		at.setMinHeight(HEIGHT);
		at.setEditable(false);
		at.setStyle("-fx-background-color:transparent;");
		repsAndWeight.getChildren().add(at);
		repsAndWeight.setPadding(new Insets(0, 5, 0, 5));
		TextField weight = new TextField();
		weight.setEditable(true);
		weight.setText("" + set.getWeight().intValue());
		weight.setMaxWidth(45);
		weight.setMinHeight(HEIGHT);
		repsAndWeight.getChildren().add(weight);
		UnitSelector units = new UnitSelector();
		units.setMinHeight(HEIGHT);
		units.setPadding(new Insets(0));
		units.getStyleClass().add("smallerCombo");
		units.valueProperty().addListener(new ChangeListener<WeightUnit>() {
			@Override
			public void changed(ObservableValue<? extends WeightUnit> obserable, WeightUnit oldValue, WeightUnit newValue) {
				set.setUnit(newValue);
			}
		});
		units.getSelectionModel().select(set.getUnitOfWeight());
		units.setMaxWidth(20);
		repsAndWeight.getChildren().add(units);
		this.setRight(deleteSet);
		this.setCenter(repsAndWeight);

		reps.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue == null || newValue.trim().equals("")) {
					set.setReps(0);
				} else {
					try {
						Integer parsedReps = Integer.parseInt(newValue);
						set.setReps(parsedReps);
					} catch (Exception e) {
						reps.setText(oldValue);
					}
				}
			}
		});
		weight.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue == null || newValue.trim().equals("")) {
					set.setWeight(0d);
				} else {
					try {
						Integer parsedWeight = Integer.parseInt(newValue);
						set.setWeight(parsedWeight.doubleValue());
					} catch (Exception e) {
						reps.setText(oldValue);
					}
				}
			}
		});
	}

	public Button getDeleteButton() {
		return this.deleteSet;
	}

}
