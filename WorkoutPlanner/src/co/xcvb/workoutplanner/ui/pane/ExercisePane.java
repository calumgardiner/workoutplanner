package co.xcvb.workoutplanner.ui.pane;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;
import co.xcvb.workoutplanner.data.Exercise;
import co.xcvb.workoutplanner.data.ExerciseType;
import co.xcvb.workoutplanner.data.lifting.WSet;
import co.xcvb.workoutplanner.ui.menu.ExerciseSelector;
import co.xcvb.workoutplanner.ui.menu.UnitsSelected;

public class ExercisePane extends BorderPane {

	private Exercise exercise;
	private ExerciseSelector selector;
	private HBox setsList;
	private Button addSetButton;
	private Button deleteExercise;

	public ExercisePane(Exercise exercise) {
		this.setPadding(new Insets(10));
		this.setHeight(50);

		this.setsList = new HBox();
		this.exercise = exercise;
		this.selector = new ExerciseSelector();
		this.selector.setMinHeight(50);
		
		this.deleteExercise = new Button("x");
		this.deleteExercise.setTextAlignment(TextAlignment.CENTER);
		this.deleteExercise.setMinWidth(15);
		this.deleteExercise.setMinHeight(50);
		this.deleteExercise.setTooltip(new Tooltip("Delete Exercise"));
		
		HBox deleteAndSelector = new HBox();
		deleteAndSelector.getChildren().add(deleteExercise);
		deleteAndSelector.getChildren().add(selector);
		this.setLeft(deleteAndSelector);
		setAlignment(selector, Pos.CENTER);
		this.selector.valueProperty().addListener(new ChangeListener<ExerciseType>() {
			@Override
			public void changed(ObservableValue<? extends ExerciseType> observable, ExerciseType oldValue,
					ExerciseType newValue) {
				exercise.setType(newValue);
			}
		});
		if (this.exercise.getType() != null) {
			this.selector.getSelectionModel().select(this.exercise.getType());
		}
		ScrollPane scrollSets = new ScrollPane(setsList);
		scrollSets.setMinHeight(50);
		scrollSets.setPadding(new Insets(0));
		scrollSets.setStyle("-fx-background-color:transparent;");
		this.setCenter(scrollSets);
		HBox.setHgrow(setsList, Priority.ALWAYS);
		if (!this.exercise.getSets().isEmpty()) {
			for (WSet set : this.exercise.getSets()) {
				addSet(set);
			}
		}
		this.addSetButton = new Button("+");
		this.addSetButton.setTextAlignment(TextAlignment.CENTER);
		this.addSetButton.setMinHeight(50);
		setAlignment(addSetButton, Pos.CENTER_RIGHT);
		this.setRight(addSetButton);
		this.addSetButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				WSet set = new WSet(0, 0d, UnitsSelected.getInstance().getUnitsSelected());
				exercise.getSets().add(set);
				addSet(set);
			}
		});
		this.addSetButton.setTooltip(new Tooltip("Add Set"));
	}

	private void addSet(WSet set) {
		SetPane setPane = new SetPane(set);
		setPane.getDeleteButton().setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				exercise.getSets().remove(set);
				setsList.getChildren().remove(setPane);
			}
		});
		setsList.getChildren().add(setPane);
	}
	
	public Button getDeleteExerciseButton() {
		return this.deleteExercise;
	}

}
