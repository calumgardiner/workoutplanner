package co.xcvb.workoutplanner.ui.pane.util;

import java.util.Comparator;

import co.xcvb.workoutplanner.data.Workout;

public class WorkoutComparator implements Comparator<Workout> {

	@Override
	public int compare(Workout o1, Workout o2) {
		return o1.getDate().compareTo(o2.getDate());
	}

}
