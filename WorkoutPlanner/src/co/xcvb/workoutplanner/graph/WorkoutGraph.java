package co.xcvb.workoutplanner.graph;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import co.xcvb.workoutplanner.data.ExerciseType;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.units.WeightUnit;

public class WorkoutGraph extends BorderPane {

	private List<Workout> workouts;
	private static final String graphApiJavascript = " <html><head><script type=\"text/javascript\" "
			+ "src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']"
			+ "}]}\"></script><script type=\"text/javascript\">google.setOnLoadCallback(drawChart);function drawChart() {var data = "
			+ "google.visualization.arrayToDataTable([?data]);var options = {title: '?title',legend: { position: 'bottom' },chartArea: "
			+ "{'width': '80%', 'height': '80%'},width: ?width,height: ?height};var chart = new google.visualization.LineChart(document.getElementById('wp_chart'));"
			+ "chart.draw(data, options);}</script></head><body><div id=\"wp_chart\"></div></body></html>";
	private static final DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
	private String tempFile = System.getProperty("user.dir") + "/resources/Graph.html";

	public WorkoutGraph(List<Workout> workouts) {
		this.workouts = workouts;
		this.setPadding(new Insets(10));
		buildGraphAndWriteToResource();
		WebView googleGraph = new WebView();
		googleGraph.getEngine().load("file:" + tempFile);
		this.setCenter(googleGraph);
	}

	private void buildGraphAndWriteToResource() {
		BufferedWriter out = null;
		try {
			String width = "700";
			String height = "700";
			
			String data = GoogleGraphWorkoutData.getMaxLiftsByDateForExercise(workouts, df.parse("2/2/2015"),
					new Date(), new ExerciseType("Squat", ""), WeightUnit.KG);
			String content = graphApiJavascript.replaceFirst("\\?data", data);
			content = content.replaceFirst("\\?data", data);
			content = content.replaceFirst("\\?title", "Squats");
			content = content.replaceFirst("\\?width", width);
			content = content.replaceFirst("\\?height", height);
			System.out.println(content);
			FileWriter fstream = new FileWriter(tempFile, false);
			out = new BufferedWriter(fstream);
			out.write(content);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception e) {
			}
		}
	}

}
