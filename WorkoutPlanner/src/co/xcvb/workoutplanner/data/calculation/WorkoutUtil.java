package co.xcvb.workoutplanner.data.calculation;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import co.xcvb.workoutplanner.data.Exercise;
import co.xcvb.workoutplanner.data.ExerciseType;
import co.xcvb.workoutplanner.data.Workout;
import co.xcvb.workoutplanner.data.lifting.WSet;
import co.xcvb.workoutplanner.data.units.WeightConverter;
import co.xcvb.workoutplanner.data.units.WeightUnit;

public class WorkoutUtil {

	/**
	 * Calculate the maximum One rep max for a particular lift between 2 dates
	 * for a specified unit.
	 * 
	 * @param type
	 * @param from
	 * @param to
	 * @param workouts
	 * @param units
	 * @return 
	 */
	public static Double get1RMForLift(ExerciseType type, Date from, Date to, List<Workout> workouts, WeightUnit units) {
		List<Workout> workoutsBetweenDates = getAllWorkoutsBetweenDates(from, to, workouts);
		Double maxRM = 0d;
		for (Workout workout : workoutsBetweenDates) {
			for (Exercise exercise : workout.getExercises()) {
				if (exercise.getType().equals(type)) {
					for (WSet set : exercise.getSets()) {
						Double oneRM = calculate1RM(set, units);
						if (oneRM > maxRM) {
							maxRM = oneRM;
						}
					}
				}
			}
		}
		return maxRM;
	}

	/**
	 * Calculate 1RM from a set.
	 * 
	 * @param set
	 * @return
	 */
	public static Double calculate1RM(WSet set, WeightUnit units) {
		Double oneRM = 0d;
		switch (set.getReps()) {
		case 1:
			oneRM = (1.0d / 1.0d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 2:
			oneRM = (1.0d / 0.95d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 3:
			oneRM = (1.0d / 0.9d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 4:
			oneRM = (1.0d / 0.88d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 5:
			oneRM = (1.0d / 0.86d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 6:
			oneRM = (1.0d / 0.83d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 7:
			oneRM = (1.0d / 0.80d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 8:
			oneRM = (1.0d / 0.78d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 9:
			oneRM = (1.0d / 0.76d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 10:
			oneRM = (1.0d / 0.75d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 11:
			oneRM = (1.0d / 0.72d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		case 12:
			oneRM = (1.0d / 0.70d * WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), units));
			break;
		default:
			oneRM = 0d;
			break;
		}
		return oneRM;
	}

	/**
	 * Calculate the heaviest lift of any particular lift between 2 dates.
	 * 
	 * @param type
	 * @param from
	 * @param to
	 * @param workouts
	 * @param units
	 * @return
	 */
	public static WSet getMaxWeightLifted(ExerciseType type, Date from, Date to, List<Workout> workouts) {
		List<Workout> workoutsBetweenDates = getAllWorkoutsBetweenDates(from, to, workouts);
		WSet heaviestSet = null;
		for (Workout workout : workoutsBetweenDates) {
			for (Exercise exercise : workout.getExercises()) {
				if (exercise.getType().equals(type)) {
					for (WSet set : exercise.getSets()) {
						if (heaviestSet == null
								|| heaviestSet.getWeight() < WeightConverter.convertWeight(set.getWeight(),
										set.getUnitOfWeight(), heaviestSet.getUnitOfWeight())) {
							heaviestSet = set;
						}
					}
				}
			}
		}
		return heaviestSet;
	}

	/**
	 * Calculate the average weight lifted for a particular lift between 2
	 * dates.
	 * 
	 * @param type
	 * @param from
	 * @param to
	 * @param workouts
	 * @param units
	 * @return the average weight lifted
	 */
	public static Double getAverageLiftBetweenDates(ExerciseType type, Date from, Date to, List<Workout> workouts,
			WeightUnit units) {
		List<Workout> workoutsBetweenDates = getAllWorkoutsBetweenDates(from, to, workouts);
		Double weight = 0d;
		Integer reps = 0;
		for (Workout workout : workoutsBetweenDates) {
			for (Exercise exercise : workout.getExercises()) {
				if (exercise.getType().equals(type)) {
					for (WSet set : exercise.getSets()) {
						reps += set.getReps();
						weight += (WeightConverter.convertWeight(set.getWeight(), set.getUnitOfWeight(), WeightUnit.KG) * set
								.getReps());
					}
				}
			}
		}
		return WeightConverter.convertWeight((weight / reps), WeightUnit.KG, units);
	}

	/**
	 * Return all workouts between 2 dates inclusive (including those dates).
	 * 
	 * @param from
	 * @param to
	 * @param workouts
	 * @return the workouts between and including to and from
	 */
	public static List<Workout> getAllWorkoutsBetweenDates(Date from, Date to, List<Workout> workouts) {
		List<Workout> workoutsBetweenDates = new ArrayList<Workout>();
		for (Workout workout : workouts) {
			if (workout.getDate().compareTo(to) == 0 || workout.getDate().compareTo(from) == 0
					|| (workout.getDate().before(to) && workout.getDate().after(from))) {
				workoutsBetweenDates.add(workout);
			}
		}
		return workoutsBetweenDates;
	}

	public static List<Workout> filterWorkoutsByExercise(List<Workout> workouts, ExerciseType type) {
		List<Workout> workoutsWithExercise = new ArrayList<Workout>();
		for (Workout workout : workouts) {
			if (workoutContainsExercise(workout, type)) {
				workoutsWithExercise.add(workout);
			}
		}
		return workoutsWithExercise;
	}

	public static boolean workoutContainsExercise(Workout workout, ExerciseType type) {
		for (Exercise e : workout.getExercises()) {
			if (e.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

	public static Map<Date, WSet> getHeaviestSetForEachDate(List<Workout> workouts, ExerciseType type) {
		Map<Date, WSet> heaviestOnDate = new LinkedHashMap<Date, WSet>();
		for (Workout workout : workouts) {
			if (workoutContainsExercise(workout, type)) {
				heaviestOnDate.put(workout.getDate(), getHeaviestSetOfExercise(workout, type));
			}
		}
		return heaviestOnDate;
	}

	public static WSet getHeaviestSetOfExercise(Workout workout, ExerciseType type) {
		WSet heaviestSet = null;
		for (Exercise e : workout.getExercises()) {
			if (e.getType().equals(type)) {
				for (WSet set : e.getSets()) {
					if (heaviestSet == null
							|| heaviestSet.getWeight() < WeightConverter.convertWeight(set.getWeight(),
									set.getUnitOfWeight(), heaviestSet.getUnitOfWeight())) {
						heaviestSet = set;
					}
				}
			}
		}
		return heaviestSet;
	}
}
